<?php

namespace App\Services;

use App\Entities\Converter\MonologueChannelEntity;
use App\Entities\Converter\SilenceChannelEntity;

class SilenceChannelService
{
    /**
     * @param SilenceChannelEntity[] $entities
     * @return MonologueChannelEntity[]
     */
    public function convertSilenceEntitiesToMonologueEntities(array $entities): array
    {
        $monologue = [];

        foreach ($entities as $key => $entity) {
            //If first element
            if ($key === 0 && $entity->start > 0.0) {
                $monologue[] = new MonologueChannelEntity(0, $entity->end);
            } else {
                $monologue[] = new MonologueChannelEntity($entities[$key - 1]->start, $entity->end);
            }
        }

        return $monologue;
    }
}
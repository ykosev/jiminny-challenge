<?php

namespace App\Services\Converter\Text;

use App\Services\Converter\ConverterInterface;

class TextConverter implements ConverterInterface
{
    /**
     * @var TextReader
     */
    private $_reader;

    /**
     * TextConverter constructor.
     */
    public function __construct()
    {
        $this->_reader = new TextReader();
    }

    /**
     * @return TextReader
     */
    public function getReader()
    {
        return $this->_reader;
    }
}
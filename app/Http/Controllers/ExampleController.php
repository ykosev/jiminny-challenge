<?php

namespace App\Http\Controllers;

use App\Services\Converter\Converter;
use App\Services\Converter\SilenceChannel\SilenceChannelConverter;
use App\Services\Converter\Text\TextConverter;
use App\Services\MonologueChannelService;
use App\Services\SilenceChannelService;

class ExampleController extends Controller
{
    public function index()
    {

        $user_channel_file = resource_path() . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'user_channel.txt';
        $customer_channel_file = resource_path() . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'customer_channel.txt';

        $converter = new Converter();
        $converter->setConverter(new TextConverter());

        $user_speech_channel = $converter->getReader()->read($user_channel_file);
        $customer_speech_channel = $converter->getReader()->read($customer_channel_file);

        $converter->setConverter(new SilenceChannelConverter());

        $user_channel_entities = $converter->getReader()->read($user_speech_channel);
        $customer_channel_entities = $converter->getReader()->read($customer_speech_channel);

        $silence_channel_service = new SilenceChannelService();

        $user_monologue_entities = $silence_channel_service->convertSilenceEntitiesToMonologueEntities($user_channel_entities);
        $customer_monologue_entities = $silence_channel_service->convertSilenceEntitiesToMonologueEntities($customer_channel_entities);

        $monologue_channel_service = new MonologueChannelService();

        $monologue_length = $monologue_channel_service->getMonologueLengthFromEntities($user_monologue_entities,
            $customer_monologue_entities);
        $user_talk_percentage = $monologue_channel_service->getMonologuePercentageFromEntities($user_monologue_entities,
            $monologue_length);

        $response_data = [
            'longest_user_monologue' => $monologue_channel_service->getLongestUnInterruptedMonologueFromEntities($user_monologue_entities,
                $customer_monologue_entities),
            'longest_customer_monologue' => $monologue_channel_service->getLongestUnInterruptedMonologueFromEntities($customer_monologue_entities,
                $user_monologue_entities),
            'user_talk_percentage' => $user_talk_percentage,
            'user' => $monologue_channel_service->convertMonologueEntitiesToArray($user_monologue_entities),
            'customer' => $monologue_channel_service->convertMonologueEntitiesToArray($customer_monologue_entities),
        ];

        return response()->json($response_data);
    }
}

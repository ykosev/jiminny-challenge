<?php

namespace App\Services\Converter\Text;

use App\Exceptions\Converter\TextConverterException;
use App\Services\Converter\ReaderInterface;

class TextReader implements ReaderInterface
{
    /**
     * @param $file
     * @return false|string
     * @throws TextConverterException
     */
    public function read($file)
    {
        $this->_checkFileExist($file);
        return $this->_readFile($file);
    }

    /**
     * @param $file
     * @throws TextConverterException
     */
    private function _checkFileExist($file)
    {
        if (!file_exists($file)) {
            throw new TextConverterException('File not exist');
        }
    }

    /**
     * @param $file
     * @return false|string
     */
    private function _readFile($file)
    {
        return file_get_contents($file);
    }
}
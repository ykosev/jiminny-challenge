<?php

namespace App\Services\Converter;

interface ConverterInterface
{
    public function getReader();
}
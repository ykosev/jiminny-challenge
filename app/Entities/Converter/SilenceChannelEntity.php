<?php

namespace App\Entities\Converter;

class SilenceChannelEntity
{
    /**
     * @var float
     */
    public $start;
    /**
     * @var float
     */
    public $end;

    public function __construct(float $start, float $end)
    {
        $this->start = $start;
        $this->end = $end;
    }
}
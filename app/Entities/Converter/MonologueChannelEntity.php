<?php

namespace App\Entities\Converter;

class MonologueChannelEntity
{
    /**
     * @var float
     */
    public $start;
    /**
     * @var float
     */
    public $end;
    /**
     * @var float
     */
    public $time;

    public function __construct(float $start, float $end)
    {
        $this->start = $start;
        $this->end = $end;
        $this->time = $end - $start;
    }

}
<?php

namespace App\Services\Converter;

class Converter
{
    /**
     * @var ConverterInterface
     */
    private $_converter;

    /**
     * @param ConverterInterface $converter
     */
    public function setConverter(ConverterInterface $converter)
    {
        $this->_converter = $converter;
    }

    /** Read method
     * @return ReaderInterface
     */
    public function getReader()
    {
        return $this->_converter->getReader();
    }
}
<?php

namespace App\Services\Converter\SilenceChannel;

use App\Services\Converter\ConverterInterface;

class SilenceChannelConverter implements ConverterInterface
{
    /**
     * @var SilenceChannelReader
     */
    private $_reader;

    /**
     * SilenceChannelConverter constructor.
     */
    public function __construct()
    {
        $this->_reader = new SilenceChannelReader();
    }

    /**
     * @return SilenceChannelReader
     */
    public function getReader()
    {
        return $this->_reader;
    }
}
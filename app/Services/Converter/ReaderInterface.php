<?php

namespace App\Services\Converter;

interface ReaderInterface
{
    public function read($data);
}
<?php

namespace App\Services;

use App\Entities\Converter\MonologueChannelEntity;

class MonologueChannelService
{
    /**
     * @param MonologueChannelEntity[] $main_entities
     * @param MonologueChannelEntity[] $compare_entities
     * @return float
     */
    public function getLongestUnInterruptedMonologueFromEntities(array $main_entities, array $compare_entities): float
    {
        $max = 0;
        $array_entities = array_values($main_entities);
        $compare_entities_collection = collect($compare_entities);

        foreach ($array_entities as $key => $entity) {

            $interruption = $compare_entities_collection->filter(function ($compare_entity) use ($entity){
                return (($compare_entity->start >= $entity->start && $compare_entity->start <= $entity->end) ||
                    ($compare_entity->end >= $entity->start && $compare_entity->end <= $entity->end));
            });

            if ($entity->time > $max && $interruption->isEmpty()) {
                $max = $entity->time;
            }

        }

        return (float)number_format($max, 2, '.', '');
    }

    /**
     * @param MonologueChannelEntity ...$entities
     * @return mixed
     */
    public function getMonologueLengthFromEntities(...$entities)
    {

        $last_entity_monologue = [];

        foreach ($entities as $entity) {
            $last_entity_monologue[] = collect($entity)->sortByDesc('end')->first();
        }

        $last_entity_collection = collect($last_entity_monologue);
        return $last_entity_collection->max('end');
    }

    /**
     * @param MonologueChannelEntity[] $entities
     * @param float $monologue_total_length
     * @return float|int
     */
    public function getMonologuePercentageFromEntities(
        array $entities,
        float $monologue_total_length
    ){

        $monologue_length = 0;

        foreach ($entities as $entity) {
            $monologue_length += $entity->time;
        }

        return number_format((($monologue_length / $monologue_total_length) * 100), 2, '.', '');
    }

    /**
     * @param MonologueChannelEntity[] $entities
     * @return array
     */
    public function convertMonologueEntitiesToArray(array $entities)
    {

        $array = [];

        foreach ($entities as $entity) {
            $array[] = [$entity->start, $entity->end];
        }

        return $array;
    }
}
## How to init project

1.Install composer dependencies

    composer install
       
2.Environment

Make a copy of  <code>env.example</code>  and rename it to <code>.env</code>
You can change the env variables to fit your server settings

 3.Serve the app
 
    php artisan serve
    
  Now you can access the web app from 
  
    http://127.0.0.1:8000

## Example API Endpoint

There is endpoint for testing purposes, accessed at

    GET http://127.0.0.1:8000/api/example
    

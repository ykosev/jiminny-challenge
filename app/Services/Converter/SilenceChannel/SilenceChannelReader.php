<?php

namespace App\Services\Converter\SilenceChannel;

use App\Entities\Converter\SilenceChannelEntity;
use App\Exceptions\Converter\SilenceChannelConverterException;
use App\Services\Converter\ReaderInterface;

class SilenceChannelReader implements ReaderInterface
{
    /**
     * @param $channel
     * @return SilenceChannelEntity[]
     * @throws SilenceChannelConverterException
     */
    public function read($channel)
    {
        $this->_validateChannel($channel);
        return $this->_read($channel);
    }

    /**
     * @param $channel
     * @throws SilenceChannelConverterException
     */
    private function _validateChannel($channel)
    {
        if (empty($channel)) {
            throw new SilenceChannelConverterException('Empty channel');
        }

        if (!is_string($channel)) {
            throw new SilenceChannelConverterException('Channel must be type of string');
        }

    }

    /**
     * @param $channel
     * @return array
     * @throws SilenceChannelConverterException
     */
    private function _read($channel)
    {

        $data = $this->_extractDataFromChannel($channel);
        return $this->_convertChannelDataToEntities($data);
    }

    /**
     * @param $channel
     * @return array
     * @throws SilenceChannelConverterException
     */
    private function _extractDataFromChannel($channel)
    {
        $data = [];

        //remove silencedetect with hash
        if (preg_match_all("/^\[silencedetect\s[^]]+]\s([^\n]+)/m", $channel, $channel_rows_matches)) {
            foreach ($channel_rows_matches[1] as $key => $channel_row) {
                //Get row numbers - start,end and time
                if (preg_match_all('!\d+(?:\.\d+)?!', $channel_row, $row_matches)) {

                    $row_match_result = $row_matches[0];

                    // If more than one result, so row with end and total time
                    if (!($key % 2)) {
                        $data['end'][] = $row_match_result[0];
                    } else {
                        $data['start'][] = $row_match_result[0];
                    }
                } else {
                    throw new SilenceChannelConverterException('Invalid channel');
                }
            }

            return $data;
        } else {
            throw new SilenceChannelConverterException('Invalid channel');
        }

    }

    /**
     * @param $data
     * @return array
     */
    private function _convertChannelDataToEntities($data)
    {
        $entities = [];
        $entities_count = count($data['start']);

        for ($i = 0; $i < $entities_count; $i++) {
            $entities[] = new SilenceChannelEntity($data['start'][$i], $data['end'][$i]);
        }

        return $entities;
    }
}